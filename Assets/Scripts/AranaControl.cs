﻿using UnityEngine;
using System.Collections;

public class AranaControl : MonoBehaviour {
	private Vector3 PosicionInicial=new Vector3(0,0,0);
	private bool Calmado=true, rotado=false;
	Animator anim;

	float smooth=1f;
	float tiempoSound=5, tiempoPasos=2;
	// Use this for initialization
	void Start () {
		PosicionInicial = transform.position;
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {


		if (tiempoSound <= 0) {
			FMODUnity.RuntimeManager.PlayOneShot ("event:/Aranas", transform.position);
			tiempoSound = Random.Range (3, 10);
		}
		tiempoSound-=Time.deltaTime;

		if (!Calmado) {
			float step = 3 * Time.deltaTime;


			transform.position = Vector3.MoveTowards (transform.position, PosicionInicial, step);
			if (!rotado) {
				transform.rotation *= Quaternion.Euler (0, 180f, 0);
				rotado = true;
			}

			if (tiempoPasos <= 0) {
				FMODUnity.RuntimeManager.PlayOneShotAttached ("event:/PasosArana", this.gameObject);
				tiempoPasos = 1.9f;
			}
			tiempoPasos -= Time.deltaTime;


		}

		if (transform.position==PosicionInicial){
			Calmado = true;
			anim.SetTrigger ("IDLE");
			if (rotado) {
				transform.rotation *= Quaternion.Euler (0, 180f, 0);
				rotado = false;
			}
		}
	}

	void OnTriggerEnter(Collider other) {
		if(other.gameObject.tag == "Vampire")
		{
			anim.SetTrigger ("Run");
			print ("Run");
			tiempoPasos = 0;
		}

	}

	void OnTriggerStay(Collider other) {
		if (other.gameObject.tag == "Vampire") {
			transform.Translate (Vector3.forward * Time.deltaTime * 5);
			anim.SetTrigger ("Run");
			if (tiempoPasos <= 0) {
				FMODUnity.RuntimeManager.PlayOneShotAttached ("event:/PasosArana", this.gameObject);
				tiempoPasos = 1.9f;
			}
			tiempoPasos -= Time.deltaTime;
		}

	}
	void OnTriggerExit(Collider other) {
		Calmado = false;
	}
	void OnCollisionEnter(Collision collision) {
		anim.SetTrigger ("Attack");
		FMODUnity.RuntimeManager.PlayOneShot ("event:/Sufrir", transform.position);
	}
}
