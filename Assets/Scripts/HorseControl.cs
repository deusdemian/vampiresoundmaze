﻿using UnityEngine;
using System.Collections;

public class HorseControl : MonoBehaviour {
	Animator anim;

	private float tiempoRelincho=0f,inicioTiempoRelincho=5f,speed=8, step=0;
	private bool cambiov=false;
	private Vector3 posicionInicial = new Vector3 (0,0,0);

	private FMOD.Studio.EventInstance galopar;


	// Use this for initialization
	void Start () {
		

		anim = GetComponent<Animator> ();
		posicionInicial = transform.position;
		step = speed * Time.deltaTime;
		galopar = FMODUnity.RuntimeManager.CreateInstance ("event:/galope");

	}
	
	// Update is called once per frame
	void Update () {
		
		if(tiempoRelincho<=0){
			

			if (cambiov) {
				FMODUnity.RuntimeManager.PlayOneShot ("event:/Relincho", transform.position);
				FMODUnity.RuntimeManager.PlayOneShotAttached ("event:/galope", this.gameObject);

				anim.SetTrigger ("Run");
				cambiov = false;
			} else {
				
				cambiov = true;
				anim.SetTrigger ("IDLE");
			}
			inicioTiempoRelincho = Random.Range (6,10);
			tiempoRelincho = inicioTiempoRelincho;
		}

		if (tiempoRelincho <= ((inicioTiempoRelincho/2)+0.1f) && !cambiov) {
			Vector3 relativePos = posicionInicial - transform.position;
			Quaternion rotation = Quaternion.LookRotation (relativePos);
			transform.rotation = rotation;
			transform.position = Vector3.MoveTowards (transform.position, posicionInicial, step);
		} else if(!cambiov){
			transform.Translate(Vector3.forward*step);
		}



		tiempoRelincho -= Time.deltaTime;
	}
}
