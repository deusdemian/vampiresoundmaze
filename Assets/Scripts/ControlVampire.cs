﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using CnControls;

public class ControlVampire : MonoBehaviour {
	private float speed = 3f,rotationSpeed=100f;
	private Vector3 posicionInicial = new Vector3 (0,0,0);
	private Quaternion rotacionInicial;
	private float tiempoBajoWater=1, tiempoSpider=1;
	private bool muerto=false;
	public Text ganar;
	private FMOD.Studio.EventInstance Aleteo;
	private FMOD.Studio.ParameterInstance velAlas;
	private Rigidbody bb;
	public float translation;
	private float ejex = 0, ejey=0;
	// Use this for initialization
	void Start () {
		Aleteo= FMODUnity.RuntimeManager.CreateInstance ("event:/Alas");
		FMODUnity.RuntimeManager.PlayOneShotAttached ("event:/ParteSuave", this.gameObject);
		Aleteo.start ();
		Aleteo.getParameter("VelAlas", out velAlas);

		posicionInicial = transform.position;
		rotacionInicial = transform.localRotation;
	}
	
	// Update is called once per frame
	void Update () {
//		velAlas.setValue(Input.GetAxis("Vertical"));
//			translation = (Input.GetAxis("Vertical")+1f) * speed;
//			float rotation = Input.GetAxis("Horizontal") * rotationSpeed;

		velAlas.setValue(CnInputManager.GetAxis("Vertical"));
		translation = (CnInputManager.GetAxis("Vertical")+1f) * speed;
		float rotation = CnInputManager.GetAxis("Horizontal")* rotationSpeed;


			translation *= Time.deltaTime;
			rotation *= Time.deltaTime;

			transform.Translate(0, 0, translation);
			transform.Rotate(0, rotation, 0);



		ejey = 0;
		ejex = 0;
		if (muerto == true ) {
			
			if (tiempoSpider <= 0) {
				tiempoSpider = 1;
				muerto = false;
				transform.position = posicionInicial;
				transform.localRotation = rotacionInicial;
			}
			tiempoSpider -= Time.deltaTime;
		}

	}
	void OnCollisionEnter(Collision collision) {
		if(collision.gameObject.tag == "Spider")
		{
			muerto = true;
		}
	}
	void OnTriggerStay(Collider other) {
		if (other.gameObject.tag == "Waterfall") {
			if (tiempoBajoWater <= 0) {
				transform.position = posicionInicial;
				transform.localRotation = rotacionInicial;
				tiempoBajoWater = 1;
			}
			tiempoBajoWater -= Time.deltaTime;
		}

	}
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Horse") {
			FMODUnity.RuntimeManager.PlayOneShotAttached ("event:/ChupaSangre", this.gameObject);
			ganar.text = "You are Fed!!";
		}
	}


	public void derecha(){
		ejex = 1;
	}
	public void izquierda(){
		ejex = -1;
	}
	public void arriba(){
		ejey = 1;
	}
	public void abajo(){
		ejey = -1;
	}


}